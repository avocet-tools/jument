package main

import (
   "os/exec"
   "path/filepath"
   "runtime"

   "github.com/spf13/cobra"
   "github.com/spf13/viper"

   "gitlab.com/avocet-tools/jument"
   "gitlab.com/avocet-tools/jument/util"
   //"github.com/charmbracelet/log"
)

const Version = "0.1.0"
var (
   cmd = cobra.Command{
      Use: "jument",
      Short: "A beast of journal burdens",
      Version: Version,
   }
   ws = cobra.Command{
      Use: "ws",
      Short: "Provides information on configured workspaces",
      Run: func(_ *cobra.Command, args []string){
      },
   }

   reportWs = cobra.Command{
      Use: "home",
      Short: "Returns the top-level directory for the given file",
      Run: func(_ *cobra.Command, args []string){
         jument.ReportWorkspace(args)
      },
   }
   readCmd = cobra.Command{
      Use: "read",
      Short: "Reads the given markdown files",
      Run: func(_ *cobra.Command, args []string){
         jument.RunRead(args)
      },
   }
   buildCmd = cobra.Command{
      Use: "build",
      Short: "Builds documents",
      Run: func(_ *cobra.Command, args []string){
         jument.RunBuild(args)
      },
   }

   reportName = cobra.Command{
      Use: "name",
      Short: "Returns the workspace name",
      Run: func(_ *cobra.Command, args []string){
         jument.ReportWorkspaceName(args)
      },
   }
   op = cobra.Command{
      Use: "open",
      Short: "Opens the index.md file in the given workspace",
      Run: func(_ *cobra.Command, args []string){
         jument.RunOpen(args)
      },
   }
)

func topLevel() (string, bool) {
   cmd := exec.Command("git", "rev-parse", "--show-toplevel")
   con, err := cmd.Output()
   if err == nil {
      return string(con), true
   }
   return "", false
}

func init(){

   viper.SetDefault("jument.database", "jument")
   viper.SetDefault("jument.local.user", "jument")
   viper.SetDefault("jument.local.pass", "jument")
   viper.SetDefault("jument.local.address", "127.0.0.1")
   viper.SetDefault("jument.local.port", 27017)
   viper.SetDefault("jument.editor", "nvim")
   viper.SetDefault("jument.tmp", "/tmp/jument")
   viper.SetDefault("jument.calendar.noCache", true)
   viper.SetDefault("jument.calendar.period", 5)
   viper.SetDefault("jument.parallel", runtime.NumCPU())

   // Read Configuration
   root, ok := topLevel()
   viper.SetConfigName("jument.yml")
   viper.AddConfigPath(".")
   if ok {
      viper.AddConfigPath(root)
   }
   viper.AddConfigPath("$HOME/.config/avocet")
   viper.SetConfigType("yaml")
   err := viper.ReadInConfig()
   if _, ok := err.(viper.ConfigFileNotFoundError); ok {
   } else if err != nil {
      panic(err)
   }
   util.Config()
   viper.SetDefault(
      "jument.cache", 
      filepath.Join(viper.GetString("jument.home"), ".local/var/jument"))
}

func main(){
   ws.AddCommand(&reportWs)
   ws.AddCommand(&reportName)
   cmd.AddCommand(&op)
   cmd.AddCommand(&ws)
   cmd.AddCommand(&readCmd)
   cmd.AddCommand(&buildCmd)
   cmd.Execute()
}
