# Jument

## Installation

```
go install gitlab.com/avocet-tools/jument
```

## Plan

* jument edit DATE - Opens entry for given date or current day.
* jument ls - Lists open tasks on current day 
* jument show DATE - Formats date and pipes results to less. 
* jument status - Shows database configuration, number of notes, etc.
* jument push - Pushes changes via mongosync to remote
* jument pull - Pulls changes via mongosync from remote
* jument ps - Provides the sync times for all instances as retrieved from remote
* jument init - Initializes database schemas and indices
* jument sync - Pulls open tasks into current log.
* jument log - Lists current tasks in future log or monthly log
* jument report - Reports recent progress
* jument ticket - Lists ticket info
* jument ticket show TICKET - Shows ticket info
* jument ticket ls - Lists all tickets


## Explaantion of the Name

The English word `jument` derives from an older Latin word `iumentum`, `iumenti` each meaning *beast of burden*.  Jument serves a similar purpose, performing brute operations to aid your reference journal. 
